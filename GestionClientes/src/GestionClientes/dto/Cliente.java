package GestionClientes.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author IPMan
 */
public class Cliente {
    private String nombre;
    private String apellidos;
    private Date fecha;
    private String provincia;

    public Cliente(String nombre, String apellidos, Date fecha, String provincia) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fecha = fecha;
        this.provincia = provincia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
    
    public String[] toArray(){
        String[] array = new String[4];
        
        array[0] = nombre;
        array[1] = apellidos;
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        array[2] = sdf.format(fecha);
        array[3] = provincia;
        
        return array;
    }
}
