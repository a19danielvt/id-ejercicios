/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author ipman
 */
public class Ejercicio7Controller implements Initializable {

    @FXML
    private Label label;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void choice(ActionEvent event) {
        List<String> options = List.of("Primero", "Segundo", "Tercero");
        ChoiceDialog dialog = new ChoiceDialog<>("Segundo", options);
        
        Optional<String> result = dialog.showAndWait();
        label.setText(result.orElse("No se ha escogido nada"));
    }
    
}
