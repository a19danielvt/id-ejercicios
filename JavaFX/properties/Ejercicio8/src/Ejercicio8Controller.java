/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author a19danielvt
 */
public class Ejercicio8Controller implements Initializable {

    @FXML
    private ToggleGroup radiogroup;
    @FXML
    private MenuItem item;
    @FXML
    private CheckBox custom;
    @FXML
    private CheckMenuItem check1;
    @FXML
    private CheckMenuItem check2;
    @FXML
    private TextArea textarea;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        item.setOnAction((t) -> {
            printMessage("Has pulsado el item.");
        });
        
        custom.setOnAction((t) -> {
            String message = "CustomMenuItem " +
                    (custom.isSelected()? "está seleccionado" : "no está seleccinado");
            printMessage(message);
        });
        
        radiogroup.selectedToggleProperty().addListener((o) -> {
            String message = 
                    ((RadioMenuItem)radiogroup.getSelectedToggle()).getText()
                    + " seleccionado";
            printMessage(message);
        });
        
        check1.setOnAction((t) -> {
            String message = "check1 " +
                    (check1.isSelected()? "está seleccionado" : "no está seleccinado");
            printMessage(message);
        });
        
        check2.setOnAction((t) -> {
            String message = "check2 " +
                    (check2.isSelected()? "está seleccionado" : "no está seleccinado");
            printMessage(message);
        });
    }    
    
    public void printMessage(String message){
        textarea.setText(textarea.getText() + message + "\n");
    }
}
