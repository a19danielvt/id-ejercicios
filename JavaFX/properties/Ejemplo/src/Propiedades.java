/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class Propiedades {
    public static void main(String[] args) {
        Bill bill = new Bill();
        
        bill.amountDueProperty().addListener((ov, t, t1) -> {
            System.out.println("Cambió la cuenta");
        });
        
        bill.setAmountDue(100.00);
    }
}
