/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author a19danielvt
 */
public class Ejercicio7Controller implements Initializable {

    @FXML
    private DatePicker date;
    @FXML
    private Label label;

    String[] meses = {
        "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", 
        "agosto", "septiembre", "octubre", "noviembre", "diciembre"
    };
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        label.setText("");
        
        date.setOnAction(new EventHandler() {
            @Override
            public void handle(Event t) {
                String fecha = date.getValue().toString();
                String[] valores = fecha.split("-");
                
                String year = valores[0];
                String day = valores[2];
                int month_value = Integer.parseInt(valores[1]) - 1;
                String month = meses[month_value];
                
                label.setText(day + " " + month + " " + year);
            }
        });
    }    
    
}
