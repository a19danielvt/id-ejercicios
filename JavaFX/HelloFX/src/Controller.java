import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Controller {
    @FXML
    private Label label1;
    
    @FXML
    public void reactToClick(Event e) {
        label1.setText("Has pulsado el botón");
    }
}
