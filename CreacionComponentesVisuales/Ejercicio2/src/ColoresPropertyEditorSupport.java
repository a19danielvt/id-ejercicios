
import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyEditorSupport;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jefe
 */
public class ColoresPropertyEditorSupport extends PropertyEditorSupport{

    private ColoresPanel panel = new ColoresPanel();
    
    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return panel;
    }

    @Override
    public String getJavaInitializationString() {
        Colores c = panel.getSelectedValue();
        Color fondo = c.getColorFondo();
        Color texto = c.getColorTexto();
        return "new Colores( new java.awt.Color(" + fondo.getRed() + ", " +
                fondo.getGreen() + ", " + fondo.getBlue() + "), " + 
                "new java.awt.Color(" + texto.getRed() + ", " +
                texto.getGreen() + ", " + texto.getBlue() + "))";
    }

    @Override
    public Object getValue() {
        return panel.getSelectedValue();
    }
    
}
