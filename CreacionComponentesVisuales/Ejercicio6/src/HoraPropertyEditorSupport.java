
import java.awt.Component;
import java.beans.PropertyEditorSupport;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jefe
 */
public class HoraPropertyEditorSupport extends PropertyEditorSupport{

    private HoraPanel panel = new HoraPanel();
    
    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return panel;
    }

    @Override
    public String getJavaInitializationString() {
        Hora hora = panel.getSelectedValue();
        return "new Hora(\"" + hora.getHora() + "\", \"" + hora.getMinuto() + "\")";
    }

    @Override
    public Object getValue() {
        return panel.getSelectedValue();
    }
    
}
