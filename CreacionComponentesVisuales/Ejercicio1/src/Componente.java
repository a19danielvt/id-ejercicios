
import java.awt.Color;
import java.io.Serializable;
import javax.swing.JButton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class Componente extends JButton implements Serializable{
    private Color colorFondo;
    private Color colorTexto;

    public Componente() {
    }

    public Color getColorFondo() {
        return colorFondo;
    }

    public void setColorFondo(Color colorFondo) {
        setBackground(colorFondo);
    }

    public Color getColorTexto() {
        return colorTexto;
    }

    public void setColorTexto(Color colorTexto) {
        setForeground(colorTexto);
    }
}
