/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionAlumnado.logica;

import gestionAlumnado.dto.Alumnado;
import java.util.ArrayList;

/**
 *
 * @author a19danielvt
 */
public class LogicaNegocio {
    
    private static ArrayList<Alumnado> alumnos = new ArrayList<>();
    
    public static void AñadirAlumno(Alumnado a){
        alumnos.add(a);
    }

    public static ArrayList<Alumnado> getAlumnos() {
        return alumnos;
    }
}
