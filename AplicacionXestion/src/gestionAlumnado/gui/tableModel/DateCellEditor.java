package gestionAlumnado.gui.tableModel;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.util.Calendar;
import java.util.Date;

/**
 * DateCellEditor that is based on {@link JSpinner} and {@link JFormattedTextField}.
 */
public class DateCellEditor extends AbstractCellEditor implements TableCellEditor {

    private JSpinner spinner;

    public DateCellEditor(String dateFormatPattern) {

        SpinnerDateModel dateModel = new SpinnerDateModel(new Date(), null, null, Calendar.DAY_OF_MONTH);
        spinner = new JSpinner(dateModel);
        spinner.setEditor(new JSpinner.DateEditor(spinner, dateFormatPattern));
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        spinner.setValue(value);
        return spinner;
    }

    @Override
    public Object getCellEditorValue() {
        return spinner.getValue();
    }

}