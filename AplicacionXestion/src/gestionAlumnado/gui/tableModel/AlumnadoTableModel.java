/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionAlumnado.gui.tableModel;

import gestionAlumnado.dto.Alumnado;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author a19danielvt
 */
public class AlumnadoTableModel extends GenericDomainTableModel<Alumnado>{
    
    public AlumnadoTableModel(ArrayList<String> columnIdentifiers){
        super(columnIdentifiers);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex){
            case 0: return String.class;
            case 1: return String.class;
            case 2: return Date.class;
        }
        throw new ArrayIndexOutOfBoundsException(columnIndex);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Alumnado a = getDomainObject(rowIndex);
        switch(columnIndex) {
            case 0: return a.getNombre();
            case 1: return a.getCurso();
            case 2: return a.getFechaAlta();
                default: throw new ArrayIndexOutOfBoundsException(columnIndex);
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Alumnado a = getDomainObject(rowIndex);
        switch(columnIndex) {
            case 0: a.setNombre((String)aValue); break;
            case 1: a.setCurso((String)aValue); break;
            case 2: a.setFechaAlta((Date)aValue); break;
                default: throw new ArrayIndexOutOfBoundsException(columnIndex);
        }
        notifyTableCellUpdated(rowIndex, columnIndex);
    }
}
