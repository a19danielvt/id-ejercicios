/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.validation.api.builtin.stringvalidation;

import org.netbeans.validation.api.Problems;
import org.openide.util.NbBundle;

/**
 *
 * @author a19danielvt
 */
public class ValidacionEdad extends StringValidator{

    @Override
    public void validate(Problems prblms, String string, String t) {
        if(t.length() == 0){
            String mensaje = NbBundle.getMessage(this.getClass(), "STRING_VACIO", string);
            prblms.add(mensaje);
        }
        
        if(t.length() == 1 && t.charAt(0) == '0'){
            String mensaje = NbBundle.getMessage(this.getClass(), "MSG_EDAD_CERO", string);
            prblms.add(mensaje);
        }

        if(t.length() > 0 && t.charAt(0) == '0'){
            String mensaje = NbBundle.getMessage(this.getClass(), "MSG_EDAD_EMPIEZA_CERO", string);
            prblms.add(mensaje);
        }

        if(t.length() > 2 && t.matches("\\d")){
            int numero = Integer.parseInt(t);
            if(numero > 120){
                String mensaje = NbBundle.getMessage(this.getClass(), "MSG_EDAD_ALTA", string);
                prblms.add(mensaje);
            }
        }
        
        if(t.length() > 1 && t.charAt(0) == '-'){
            String mensaje = NbBundle.getMessage(this.getClass(), "MSG_NUMERO_NEGATIVO", string);
            prblms.add(mensaje);
        }
        
        if(!t.matches("\\d")){
            String mensaje = NbBundle.getMessage(this.getClass(), "NO_ES_ENTERO", string);
            prblms.add(mensaje);
        }
    }
    
}
