/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpanelimaxe;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author ipman
 */
public class JPanelImaxe extends JPanel implements Serializable{
    private ImaxeFondo imaxeFondo;
    private boolean ratonPresionado = false;
    private Point puntoPresion;
    private ArrastreListener arrastreListener;

    public JPanelImaxe() {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (ratonPresionado) {
                    Point posicionActual = e.getPoint();
                    if (Math.abs(puntoPresion.x - posicionActual.x) >= 50) {
                        if (arrastreListener != null) {
                            arrastreListener.arrastre();
                        }
                    }
                }
                ratonPresionado = false;
            }
            
            @Override
            public void mousePressed(MouseEvent e) {
                ratonPresionado = true;
                puntoPresion = e.getPoint();
            }
        });
    }
    
    public void addArrastreListener(ArrastreListener arrastreListener){
        this.arrastreListener = arrastreListener;
    }
    
    public void removeArrastreListener(){
        this.arrastreListener = null;
    }

    public ImaxeFondo getImaxeFondo() {
        return imaxeFondo;
    }

    public void setImaxeFondo(ImaxeFondo imaxeFondo) {
        this.imaxeFondo = imaxeFondo;
        
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); 
        if (imaxeFondo != null) {
            if (imaxeFondo.getRutaImaxe() != null && imaxeFondo.getRutaImaxe().exists()) {
                ImageIcon imageIcon = new ImageIcon(imaxeFondo.getRutaImaxe().getAbsolutePath());
                Graphics2D g2d = (Graphics2D) g;
                g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                        imaxeFondo.getOpacidade()));
                g.drawImage(imageIcon.getImage(), 0, 0, null);
                // Unha vez cambiada la opacidad, hay que volver a ponerla en 1
                g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1));
            }
        }
    }    
}
